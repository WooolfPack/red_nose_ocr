from PIL import Image
import pytesseract
import cv2
import os
import re
import glob
import pandas as pd
import json
import logging


with open("config.json", 'r') as f:  # args.a
	cfg = json.load(f)

logging.basicConfig(filename=cfg["log_path"], filemode='w', format='%(name)s - %(levelname)s - %(message)s')

# from argparse import ArgumentParser


def preprocess_image(image_path, preprocessed_imgs_path, preprocess_type=None, grayscale=True, name_prefix="0"):
	"""
	Preprocessing of images.
	:param image_path: path to the image
	:param preprocessed_imgs_path: folder for storage of preprocessed image cuts
	:param preprocess_type: type of preprocessing, "thresh" for threshold method
	:param grayscale: boolean, True if convert to greyscale
	:param name_prefix: prefix for preprocessed image cuts names
	:return: list of files (image cuts)
	"""
	# load the example image
	image = cv2.imread(image_path)

	x1 = cfg["x1"]
	dx = cfg["dx"]
	dy = cfg["dy"]
	ddy = cfg["ddy"]
	title = cfg["title_page"]
	if title is True and "_0001." in image_path:
		y1 = cfg["y1_title_page"]
		no = cfg["no_of_records_title_page"]
	else:
		y1 = cfg["y1"]
		no = cfg["no_of_records"]

	# crop image
	image_lst = []
	if no > 0:
		image_lst.append(image[y1:y1 + dy, x1:x1 + dx])
	if no > 1:
		image_lst.append(image[y1 + ddy + dy:y1 + ddy + 2 * dy, x1:x1 + dx])
	if no > 2:
		image_lst.append(image[y1 + 2 * ddy + 2 * dy:y1 + 2 * ddy + 3 * dy, x1:x1 + dx])
	if no > 3:
		image_lst.append(image[y1 + 3 * ddy + 3 * dy:y1 + 3 * ddy + 4 * dy, x1:x1 + dx])
	if no > 4:
		image_lst.append(image[y1 + 4 * ddy + 4 * dy:y1 + 4 * ddy + 5 * dy, x1:x1 + dx])
	if no > 5:
		image_lst.append(image[y1 + 5 * ddy + 5 * dy:y1 + 5 * ddy + 6 * dy, x1:x1 + dx])
	if no > 6:
		image_lst.append(image[y1 + 6 * ddy + 6 * dy:y1 + 6 * ddy + 7 * dy, x1:x1 + dx])
	if no > 7:
		image_lst.append(image[y1 + 7 * ddy + 7 * dy:y1 + 7 * ddy + 8 * dy, x1:x1 + dx])

	# convert it to grayscale
	if grayscale:
		image_lst = [cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) for img in image_lst]

	# thresholding
	if preprocess_type == "thresh":
		image_lst = [cv2.threshold(img, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1] for img in image_lst]
	# median blurring
	elif preprocess_type == "blur":
		image_lst = [cv2.medianBlur(img, 3) for img in image_lst]

	# write the grayscale image to disk as a temporary file so we can apply OCR to it
	filename_lst = []
	i = 0
	for img in image_lst:
		name = os.path.join(preprocessed_imgs_path, "{}.png".format(name_prefix + "_" + str(i)))
		i += 1
		cv2.imwrite(name, img)
		filename_lst.append(name)

	return filename_lst


def ocr(filename, tesseract_exe_path):
	"""
	Perform tesseract OCR on an image.
	:param filename: file path to image to be OCR-ed
	:param tesseract_exe_path: path where Tesseract exe can be found
	:return: OCR data - text with confidences
	"""
	# load the image as a PIL/Pillow image, apply OCR
	pytesseract.pytesseract.tesseract_cmd = tesseract_exe_path
	text_data = pytesseract.image_to_data(
		Image.open(filename),
		output_type='data.frame',
		config='--psm 4 '
		'--oem 3 '
		'--user-patterns user-patterns '
		'-c tessedit_char_whitelist=0123456789,.')
	text_data = text_data[["conf", "text"]].dropna()

	return text_data


def create_output(text_data_lst, conf_threshold):
	"""
	Parse OCR output.
	:param text_data_lst: Tesseract OCR text data
	:param conf_threshold: threshold confidence level to mark for check
	:return: list of dicts, where each dict belongs to one record - symbol, value
	"""
	output = list()
	for filename, i in text_data_lst.items():
		i = i.reset_index()
		record = dict()
		try:
			record["symbol"] = i['text'].iloc[0]  # 1
			record["value"] = i['text'].iloc[1]  # 2
		except Exception:
			record["symbol"] = "NA"
			record["value"] = "NA"

		record["source"] = filename.split("\\")[-1]
		try:
			if i['conf'].iloc[0] < conf_threshold[0] or not re.match('^\d{10}$', i['text'].iloc[0]):
				record["check_symbol"] = "Yes"
			if i['conf'].iloc[1] < conf_threshold[1] or not re.match('\d+\,\d{2}$', i['text'].iloc[1]):
				record["check_value"] = "Yes"
		except Exception:
			record["check_symbol"] = "Yes"
			record["check_value"] = "Yes"
		output.append(record)
	return output


def line_prepender(filename, line):
	"""
	Add special line to the beginning of file.
	:param filename: a file where extra first line is tobe added
	:param line: line - string to be prepended
	"""
	with open(filename, 'r+') as f:
		content = f.read()
		f.seek(0, 0)
		f.write(line.rstrip('\r\n') + '\n' + content)


if __name__ == "__main__":
	# parser = ArgumentParser()
	# parser.add_argument('-a')
	# args = parser.parse_args()

	try:
		files = glob.glob(os.path.join(cfg["preprocess_img_path"], "*"))
		for f in files:
			os.remove(f)

		output_lst = []
		i = 0
		input_folder = cfg["input_images_path"]
		for file in os.listdir(input_folder):
			file = os.path.join(input_folder, file)
			print(file)
			image_lst = preprocess_image(
				file,
				cfg["preprocess_img_path"],
				grayscale=cfg["greys/cale"],
				preprocess_type=cfg["preprocess_type"],
				name_prefix=str(i))
			i += 1

			text_data_lst = {}
			for img in image_lst:
				print(img)
				text_data_lst[img] = ocr(img, cfg["tesseract_exe_path"])

			output_lst += create_output(
				text_data_lst,
				(cfg["symbol_confidence_threshold"], cfg["value_confidence_threshold"]))

		output_df = pd.DataFrame(output_lst)
		output_df.to_csv(cfg["output_file_path"], index=False, sep=";")
		line_prepender(cfg["output_file_path"], "sep=;")
	except Exception as e:
		logging.exception('Error encountered.')
