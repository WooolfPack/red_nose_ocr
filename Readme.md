## Červený Nos - Clown Doctors

Simple script for reading donation statement scans based on Optical Character Recognition (OCR). OCR part is powered by [Tesseract Open Source OCR Engine](https://github.com/tesseract-ocr/tesseract) and its python wrapper [pytesseract](https://pypi.org/project/pytesseract/).

More info about [Červený Nos](https://www.cervenynos.sk/) :red_circle:.
